```javascript
import Button from 'componentLib/button';
```

### Gray
    initialState = {isLoading: false};
    const clickHandler = function() {
        setState({isLoading: true});
        console.log('Clicked the button form the click handler.');
        setTimeout(()=>{setState({isLoading: false});},5000);
    };
    <div className="flex">
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler}>Gray</Button>
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler} rounded>Rounded</Button>
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler} outline>Outline</Button>
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler} rounded outline>Rounded Outline</Button>
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler} link>Link</Button>
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler} rounded link>Rounded Link</Button>
    </div>

### Primary
    <div className="flex">
        <Button color="blue" className="mar-left-05r" >Primary</Button>
        <Button color="blue" className="mar-left-05r" rounded>Rounded</Button>
        <Button color="blue" className="mar-left-05r" outline>Outline</Button>
        <Button color="blue" className="mar-left-05r" rounded outline>Rounded Outline</Button>
        <Button color="blue" className="mar-left-05r" link>Link</Button>
        <Button color="blue" className="mar-left-05r" rounded link>Rounded Link</Button>
    </div>

### Info
    <div className="flex">
        <Button color="mint" className="mar-left-05r" >Info</Button>
        <Button color="mint" className="mar-left-05r" rounded>Rounded</Button>
        <Button color="mint" className="mar-left-05r" outline>Outline</Button>
        <Button color="mint" className="mar-left-05r" rounded outline>Rounded Outline</Button>
        <Button color="mint" className="mar-left-05r" link>Link</Button>
        <Button color="mint" className="mar-left-05r" rounded link>Rounded Link</Button>
    </div>

### Warning
    <div className="flex">
        <Button color="yellow" className="mar-left-05r" >Warning</Button>
        <Button color="yellow" className="mar-left-05r" rounded>Rounded</Button>
        <Button color="yellow" className="mar-left-05r" outline>Outline</Button>
        <Button color="yellow" className="mar-left-05r" rounded outline>Rounded Outline</Button>
        <Button color="yellow" className="mar-left-05r" link>Link</Button>
        <Button color="yellow" className="mar-left-05r" rounded link>Rounded Link</Button>
    </div>

### Danger
    <div className="flex">
        <Button color="red" className="mar-left-05r" >Danger</Button>
        <Button color="red" className="mar-left-05r" rounded>Rounded</Button>
        <Button color="red" className="mar-left-05r" outline>Outline</Button>
        <Button color="red" className="mar-left-05r" rounded outline>Rounded Outline</Button>
        <Button color="red" className="mar-left-05r" link>Link</Button>
        <Button color="red" className="mar-left-05r" rounded link>Rounded Link</Button>
    </div>

### Success
    <div className="flex">
        <Button color="green" className="mar-left-05r" >Success</Button>
        <Button color="green" className="mar-left-05r" rounded>Rounded</Button>
        <Button color="green" className="mar-left-05r" outline>Outline</Button>
        <Button color="green" className="mar-left-05r" rounded outline>Rounded Outline</Button>
        <Button color="green" className="mar-left-05r" link>Link</Button>
        <Button color="green" className="mar-left-05r" rounded link>Rounded Link</Button>
    </div>

### Loading
    <Button loading={true} className="latest">Loading</Button>
### Circular
    initialState = {isLoading: false};
    const clickHandler = function() {
        setState({isLoading: true});
        console.log('Clicked the button form the click handler.');
        setTimeout(()=>{setState({isLoading: false});},5000);
    };
    <div className="flex">
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler} color="green" rounded>
            <Icon name="check" size="12" color="white"/>
        </Button>
        <Button className="mar-left-05r" loading={state.isLoading} onClick={clickHandler} rounded color="red">
            <Icon name="close" size="12" color="white"/>
        </Button>
    </div>

### Disabled
    <Button disabled={true} >Disabled</Button>
### Sizes
    <div>
        <Button color="red" className="mar-left-05r" size='small' method='replace'>Small</Button>
        <Button color="yellow" className="mar-left-05r" size='regular' method='replace'>Regular</Button>
        <Button color="green" className="mar-left-05r" size='large' method='replace'>Large</Button>
    </div>
### Block
    <Button color="mint" fullWidth={true} size="large" href="https://www.exactuals.com">Large full width</Button>




