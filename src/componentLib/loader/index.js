import React from 'react';

const Loader = () => (
	<div className="loader">
		<span className="circle circle1" />
		<span className="circle circle2" />
		<span className="circle circle3" />
	</div>

);
export default Loader;
