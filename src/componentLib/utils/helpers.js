import React from 'react';

const idMap = {};
const singletonMap = {};
const isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
let intialized = false;
let toolTipBox = document.querySelector('#tt_box');
const Helpers = {

	/**
	* @return Boolean whether current browser is IE11 or not.
	*/
	isBrowserIE11 () {
		return isIE11;
	},

	/**
	 * @param {React Component} component
	 * @param {Object with key value as the attributes to be added} attrObj
	 * @return Same componenet with all the attributes added
	 */
	addAdditionalAttributes (component, attrObj) {
		return React.cloneElement(component, attrObj);
	},

	/**
	 * Retrieves a unique ID for a specific type of Component
	 * @param {React component} componentName
	 */
	getComponentId (componentName) {
		idMap[componentName] = idMap[componentName] || 0;
		return '_' + componentName + idMap[componentName]++;
	},

	/**
	 * @param {React Component} component
	 * @param {Object with key value as the attributes to be added} attrObj
	 * @return Same componenet with all the attributes added enclosed in a div
	 */
	wrapAndAddAdditionalAttributes (component, attrObj) {
		const { children } = component.props;
		return (<div {...attrObj}>{children}</div>);
	},

	getTooltipBox () {
		if (!toolTipBox) {
			const df = document.createDocumentFragment();
			toolTipBox = document.createElement('div');
			const chevron = document.createElement('div');
			chevron.classList.add('absolute', 'chevron');
			const text = document.createElement('span');
			toolTipBox.appendChild(text);
			text.classList.add('overflow-hidden', 'justify-content-ce', 'text-center');
			toolTipBox.appendChild(chevron);
			toolTipBox.setAttribute('id', 'tt_box');
			toolTipBox.classList.add('absolute', 'pad-05r', 'hidden', 'opacity0', 'white', 'ft-sz-14', 'tooltip');
			df.appendChild(toolTipBox);
			document.body.appendChild(df);
		}
        toolTipBox.classList.add('top-z-index');
		return toolTipBox;
	},

	singleton (name, component) {
		if (singletonMap[name]) {
			return null;
		} else {
			singletonMap[name] = component;
			return singletonMap[name];
		}
	},

	loadAssetsOnce () {
		if (!intialized) {
			// require('@collabui/collab-ui/css/collab-ui.css');
			intialized = true;
		}
	}
};

export default Helpers;
