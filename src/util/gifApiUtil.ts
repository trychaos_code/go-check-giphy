import {GIPHY_CRED} from "../smiley/smiley";

export const getFromGiphy = (searchKey, cb) => {
    let url = searchKey
        ? `${GIPHY_CRED.giphy_url.search}?api_key=${GIPHY_CRED.api_key}&q=${searchKey}`
        : `${GIPHY_CRED.giphy_url.trending}?api_key=${GIPHY_CRED.api_key}`;
    let smileyArrText = '';
    window.fetch(url)
        .then(res=>res.json())
        .then(res=>{
            res.data.forEach((i, ind)=>{
                smileyArrText += `
                    <div class="clickable-gif-parent" data-title="${i.title}">
                        <input class="tog-img display-none start-inp-data" 
                        type="radio" 
                        name="original_still_${ind}" 
                        id="original_still_${ind}_1" 
                        value="1">
                        <label class="clickable-gif" style="float: left;" for="original_still_${ind}_1">
                           <img class="clickable-gif"
                                src="${i.images.original_still.url}" 
                            />
                            <div class="handler">+</div>
                            <div class="handler-1">Play</div>
                        </label>
                        <input class="tog-img display-none" 
                        type="radio" 
                        name="original_still_${ind}" 
                        id="original_still_${ind}_2" 
                        value="2" checked>
                        <label class="clickable-gif" style="float: left;" for="original_still_${ind}_2">
                            <video class="clickable-gif" autoplay loop>
                                <source src="${i.images.original_mp4.mp4}" type="video/mp4">
                            </video>
                            <div class="handler">+</div>
                            <div class="handler-1">Pause</div>
                        </label>
                    </div>`;
            });
            cb(smileyArrText);
        });
};
