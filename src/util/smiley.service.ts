import {smiley} from "../smiley/smiley"
import {SillySearch} from "./SillySearch"

const MAX_SMILEY = 80;

interface SmileyObj {
    keyArr: any,
    silly: SillySearch
}

export class SmileyService {
    smileyObj: SmileyObj = {
        keyArr: [],
        silly: new SillySearch()
    };

    constructor() {
        this.getSmileyTrie();
    }

    private getSmileyTrie() {
        Object.keys(smiley).forEach(k => {
            this.smileyObj.keyArr.push(k);
        });
    }

    search(key, type) {
        let searched = key && this.smileyObj.silly.filter(key, this.smileyObj.keyArr).reduce((a, b) => {
            // not using score, html, index now
            a[b.original] = true;
            return a;
        }, {});
        // @ts-ignore
        return Object.entries(smiley).filter(([k, v]) => {
            return v.type === type && (!key || searched[k])
        }).map(([k, v]) => {
            // @ts-ignore
            const {val} = v;
            return {k, v: val}
        }).slice(0, MAX_SMILEY);
    }
}
