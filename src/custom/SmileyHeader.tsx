import React, {Fragment} from "react";
import cx from "classnames";
import {ASSET_PATH} from "../smiley/smiley";

import Button from "../componentLib/button";

export const SmileyGroup = props => (
    <Fragment>
        {
            Object.entries(ASSET_PATH)
                .map(([k, v]) => (
                        <Button
                            key={k}
                            color="mint"
                            fullWidth={true}
                            className={
                                cx('mar-bottom-05r text-center', {
                                    'btn-active': props.viewType === k,
                                    'btn-sm-font': k === "0"
                                })
                            }
                            onClick={() => props.searchSmiley('viewType', k)}
                            outline
                        >
                            {
                                v.g.includes('.png')
                                    ? <img src={v.g} alt={v.g}/>
                                    : v.g

                            }
                        </Button>
                    )
                )
        }
    </Fragment>
)
