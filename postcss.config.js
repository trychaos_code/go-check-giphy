module.exports = {
    plugins: [
        require('postcss-nested'),
        require('autoprefixer'),
        require('postcss-smart-import'),
        require('precss'),
        require('postcss-nested-props'),
        require('postcss-short'),
        require('postcss-utilities'),
        require('autoprefixer'),
        require('postcss-preset-env'),
        require('postcss-sorting'),
        require('postcss-autoreset'),
        require('postcss-mixins'),
        require('postcss-advanced-variables'),
    ],
};
