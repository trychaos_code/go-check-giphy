const fs = require('fs'),
      path = require('path');

const ASSET_TYPE = {
  TEXT_EMOJI: "0",
  TEXT_EMOTICONS: "1",
  STATIC_CARTOON: "2",
  STATIC_PEOPLE: "3",
  DYNAMIC_CUTE: "4",
  DYNAMIC_MINION: "5"
};
const REV_ASSET_TYPE = Object.keys(ASSET_TYPE).reduce((a,b)=>{
  a[ASSET_TYPE[b]] = b;
  return a;
},{});

const AVAILABLE_KEYS = {
  valid: Object.values(ASSET_TYPE),
  text: [ASSET_TYPE.TEXT_EMOJI, ASSET_TYPE.TEXT_EMOTICONS],
  static: [ASSET_TYPE.STATIC_CARTOON, ASSET_TYPE.STATIC_PEOPLE],
  dynamic: [ASSET_TYPE.DYNAMIC_CUTE, ASSET_TYPE.DYNAMIC_MINION]
};

const giphy_url = {
  trending: 'https://api.giphy.com/v1/gifs/trending',
  search: 'https://api.giphy.com/v1/gifs/search'
};

const PUBLIC_PTH = {
  TEXT_EMOJI: '/giphy/text/emoji.json',
  TEXT_EMOTICONS: '/giphy/text/emoticons.json',
  STATIC: '/giphy/static/pngs/',
  DYNAMIC_CUTE: '/giphy/dynamic/cute/',
  DYNAMIC_MINION: '/giphy/dynamic/minion/',
  ONLINE_GIF: giphy_url,
};

const ASSET_PATH = {
  [ASSET_TYPE.TEXT_EMOJI]: {g: '😀', p: PUBLIC_PTH.TEXT_EMOJI},
  [ASSET_TYPE.TEXT_EMOTICONS]: {g: '︻╦╤─', p: PUBLIC_PTH.TEXT_EMOTICONS},
  [ASSET_TYPE.STATIC_CARTOON]: {g: PUBLIC_PTH.STATIC+'together,we.png', p: PUBLIC_PTH.STATIC},
  [ASSET_TYPE.STATIC_PEOPLE]: {g: PUBLIC_PTH.STATIC+'bow.png', p: PUBLIC_PTH.STATIC},
  [ASSET_TYPE.DYNAMIC_CUTE]: {g: '/giphy/dynamic/cute/__icon.png', p: PUBLIC_PTH.DYNAMIC_CUTE},
  [ASSET_TYPE.DYNAMIC_MINION]: {g: '/giphy/dynamic/minion/__icon.png', p: PUBLIC_PTH.DYNAMIC_MINION},
  [ASSET_TYPE.ONLINE_GIF]: {g: '/giphy/static/pngs/gif.png', p: PUBLIC_PTH.ONLINE_GIF},
};

const setFile = (file, data)=> fs.writeFileSync(
  path.resolve(__dirname, file),
  data,
  'utf8'
);

const getFile = (file, type)=> {
  const filPth = path.resolve(__dirname, file);
  let data = JSON.parse(fs.readFileSync(filPth, 'utf8'));
  if(AVAILABLE_KEYS.static.includes(type)){
    let updated = false;
    Object.keys(data).forEach(i=>{
      const pngPath = path.resolve(__dirname, './public/giphy/static/pngs/'+i+'.png');
      if (!fs.existsSync(pngPath)) {
        delete data[i];
        updated = true;
      }
    });
    updated && setFile(filPth, JSON.stringify(data, null, 2));
  }
  return data;
};

const __dataPath = {
  [ASSET_TYPE.TEXT_EMOJI]: getFile('./public/giphy/text/emoji.json', ASSET_TYPE.TEXT_EMOJI),
  [ASSET_TYPE.TEXT_EMOTICONS]: getFile('./public/giphy/text/emoticons.json', ASSET_TYPE.TEXT_EMOTICONS),

  [ASSET_TYPE.STATIC_CARTOON]: getFile( './public/giphy/static/json/cartoon.json', ASSET_TYPE.STATIC_CARTOON),
  [ASSET_TYPE.STATIC_PEOPLE]: getFile( './public/giphy/static/json/people.json', ASSET_TYPE.STATIC_PEOPLE),

  [ASSET_TYPE.DYNAMIC_CUTE]: getFile( './public/giphy/dynamic/cute/desc.json', ASSET_TYPE.DYNAMIC_CUTE),
  [ASSET_TYPE.DYNAMIC_MINION]: getFile( './public/giphy/dynamic/minion/desc.json', ASSET_TYPE.DYNAMIC_MINION)
};
const outputJsn = {},
  assetsKeys = Object.keys(ASSET_TYPE);
let dynamicCss = '';
Object.values(ASSET_TYPE).map(i=>({
  data: __dataPath[i],
  type: i,
})).forEach((i)=>{
  Object.keys(i.data).forEach(key=>{
    let vl = {type: i.type, val: __dataPath[i.type][key]};
    vl = vl.val === -1 ? {type: i.type} : vl;
    if(outputJsn[key]){
      const splitV = assetsKeys[i.type].split('_');
      if(splitV[0]!== 'STATIC'){
        outputJsn[key+'>'+splitV[1].toLowerCase()] = vl;
      }
    } else {
      outputJsn[key] = vl;
    }
  });
  if(AVAILABLE_KEYS.dynamic.includes(i.type)){
    let dirN;
    switch (i.type) {
      case ASSET_TYPE.DYNAMIC_CUTE:
        dirN = "cute";
        break;
      case ASSET_TYPE.DYNAMIC_MINION:
        dirN = "minion";
        break;
      default:
        break
    }
    getDynamicCss(dirN, i.type, i.data);
  }
});

ASSET_TYPE.ONLINE_GIF = ''+(Math.max.apply(null, Object.values(ASSET_TYPE).map(i=>parseInt(i)))+1);

Object.entries(outputJsn).forEach(([k,v])=>{
  if(!AVAILABLE_KEYS.static.includes(v.type)){
    const pngPath = path.resolve(__dirname, './public/giphy/static/pngs/'+k+'.png');
    if (fs.existsSync(pngPath)) {
      fs.unlinkSync(pngPath)
    }
  }
});

REV_ASSET_TYPE[ASSET_TYPE.ONLINE_GIF] = "ONLINE_GIF";
setFile('./src/smiley/smiley.ts',
  `export const smiley = ${JSON.stringify(outputJsn, null, 2)};
export const ASSET_TYPE = ${JSON.stringify(ASSET_TYPE, null, 2)};
export const REV_ASSET_TYPE = ${JSON.stringify(REV_ASSET_TYPE, null, 2)};
export const ASSET_PATH = ${JSON.stringify(ASSET_PATH, null, 2)};
export const GIPHY_CRED = ${JSON.stringify({
    api_key: 'nfFCd4ckhX5P8Zvm5x4JTCGlW7Kg57HS', giphy_url
  }, null, 2)};
export const getDynamicKey = (dirName, _key) => { 
  return (dirName +'_'+ _key.split(/[^a-zA-Z0-9]+/g).join('_')).toLowerCase();
}
export const AVAILABLE_KEYS = ${JSON.stringify(AVAILABLE_KEYS)}
`);

setFile('./public/giphy/dynamic/dynamicCss.css', dynamicCss);
function getDynamicCss(dirName, kyName, p) {
  const round = num => Math.round(num);
  Object.entries(p).forEach(([k,v])=>{
    const keyName = (dirName +'_'+ k.split(/[^a-zA-Z0-9]+/g).join('_')).toLowerCase();
    const {dim, n} = v;
    let _w = round(dim[0]/n[0]),
        _h = round(dim[1]/n[1]);
    const reduceFactor = 100/_h;
    _w = _w * reduceFactor;
    _h = 100;
    dynamicCss += `
.${keyName} {
  --w:${_w}px;
  --h:${_h}px;
  --n:${n[1]};
  --m:${n[0]};
  --d:2s;
  display: inline-block;
  cursor: pointer;
  width: var(--w);
  height: var(--h);
  background-image: url('${ASSET_PATH[kyName].p}${k}.png');
  background-size: calc(var(--m)*var(--w)) calc(var(--n)*var(--h));
  animation-name:  ${keyName}-x, ${keyName}-y; 
  animation-duration: calc(var(--d)/var(--n)),var(--d);
  animation-timing-function:steps(var(--m)),steps(var(--n));
  animation-iteration-count:infinite;
}

@keyframes ${keyName}-x {
  100% {
    background-position-x: calc(-1*var(--m)*var(--w));
  }
}
@keyframes ${keyName}-y {
  100% {
    background-position-y: calc(-1*var(--n)*var(--h));
  }
}`
});
}
